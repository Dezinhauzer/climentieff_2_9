#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include<stdio.h>
#include<errno.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<sys/types.h>
#include<sys/stat.h>

/**
 * Варинат 9 BEKN
 *  B – потоки одного процесса;
 *  E – условные переменные;
 *  K – очереди сообщений;
 *  N – выборочная дисперсия трех чисел:
 */

pthread_cond_t mainCondition, plusCondition, mulCondition, divisionCondition;
pthread_mutex_t mainMutex, plusMutex, mulMutex, divisionMutex;

#define TYPE 12345

struct msgBuffer {
    long type;
    char msg[50];
};

double CalculateThread(double a, double b, pthread_cond_t &plusCondition, pthread_mutex_t &plusMutex);

void Sender(double digit) {
    struct msgBuffer buf;
    int q;
    key_t t;
    t = ftok(".", 0x12);
    q = msgget(t, 0666 | IPC_CREAT);
    buf.type = TYPE;

    auto digitString = std::to_string(digit);
    for (int i = 0; i < digitString.length(); i++) {
        buf.msg[i] = digitString[i];
    }
    buf.msg[digitString.length()] = 0;

    msgsnd(q, &buf, sizeof(struct msgBuffer) - sizeof(long), 0);
}

double Receiver() {
    int t, q, n;
    struct msgBuffer buf;
    struct msqid_ds d;
    t = ftok(".", 0x12);
    q = msgget(t, 0666);
    n = msgrcv(q, &buf, sizeof(struct msgBuffer) - sizeof(long),
               TYPE, MSG_NOERROR | IPC_NOWAIT);

    n = msgctl(q, IPC_RMID, NULL);
    return std::stod(buf.msg);
}


void *plusThreadFunction(void *arg) { /* Поток 1 */
    std::cout << "Поток сложения активен" << std::endl;
    pthread_cond_signal(&plusCondition);

    while (1) {
        pthread_cond_wait(&plusCondition, &plusMutex);
        double a = Receiver();
        pthread_cond_signal(&plusCondition);

        pthread_cond_wait(&plusCondition, &plusMutex);
        double b = Receiver();
        double c = a + b;
        std::cout << a << " + " << b << " = " << c << std::endl;
        Sender(c);

        pthread_cond_signal(&plusCondition);
    }

    return nullptr;
}

void *mulThreadFunction(void *arg) { /* Поток 2 */
    std::cout << "Поток умножения активен" << std::endl;
    pthread_cond_signal(&mulCondition);

    while (1) {
        pthread_cond_wait(&mulCondition, &mulMutex);
        double a = Receiver();
        pthread_cond_signal(&mulCondition);

        pthread_cond_wait(&mulCondition, &mulMutex);
        double b = Receiver();
        double c = a * b;
        std::cout << a << " * " << b << " = " << c << std::endl;
        Sender(c);

        pthread_cond_signal(&mulCondition);
    }
}

void *divisionThreadFunction(void *arg) { /* Поток 2 */

    std::cout << "Поток деления активен" << std::endl;
    pthread_cond_signal(&divisionCondition);

    while (1) {
        pthread_cond_wait(&divisionCondition, &divisionMutex);
        double a = Receiver();
        pthread_cond_signal(&divisionCondition);

        pthread_cond_wait(&divisionCondition, &divisionMutex);
        double b = Receiver();
        double c = a / b;
        std::cout << a << " / " << b << " = " << c << std::endl;
        Sender(c);

        pthread_cond_signal(&divisionCondition);
    }
}


int main() {
    int result, plusStatus, mulStatus, divisionStatus;
    pthread_t plusThread, mulThread, divisionThread;

    pthread_cond_init(&mainCondition, NULL);
    pthread_cond_init(&plusCondition, NULL);
    pthread_cond_init(&mulCondition, NULL);
    pthread_cond_init(&divisionCondition, NULL);

    pthread_mutex_init(&mainMutex, {});
    pthread_mutex_init(&plusMutex, {});
    pthread_mutex_init(&mulMutex, {});
    pthread_mutex_init(&divisionMutex, {});

    pthread_create(&plusThread, NULL, plusThreadFunction, NULL);
    pthread_cond_wait(&plusCondition, &plusMutex);

    pthread_create(&mulThread, NULL, mulThreadFunction, NULL);
    pthread_cond_wait(&mulCondition, &mulMutex);

    pthread_create(&divisionThread, NULL, divisionThreadFunction, NULL);
    pthread_cond_wait(&divisionCondition, &divisionMutex);


    double a, b, c;
    std::cout << "Введите x1: ";
    std::cin >> a;

    std::cout << "Введите x2: ";
    std::cin >> b;

    std::cout << "Введите x3: ";
    std::cin >> c;

    double x12 = CalculateThread(a, b, plusCondition, plusMutex);
    double x123 = CalculateThread(x12, c, plusCondition, plusMutex);
    double x123div3 = CalculateThread(x123, 3, divisionCondition, divisionMutex);

    double first = CalculateThread(a, -x123div3, plusCondition, plusMutex);
    first = CalculateThread(first, first, mulCondition, mulMutex);

    double second = CalculateThread(b, -x123div3, plusCondition, plusMutex);
    second = CalculateThread(second, second, mulCondition, mulMutex);

    double third = CalculateThread(c, -x123div3, plusCondition, plusMutex);
    third = CalculateThread(third, third, mulCondition, mulMutex);

    first = CalculateThread(first, second, plusCondition, plusMutex);
    first = CalculateThread(first, third, plusCondition, plusMutex);

    double div = CalculateThread(first, 2, divisionCondition, divisionMutex);

    std::cout << "Мат ожидание: " << x123div3 << std::endl;
    std::cout << "Дисперсия: " << div << std::endl;

    printf("\nПотоки завершены с %d и %d и %d\n");
}

double CalculateThread(double a, double b, pthread_cond_t &plusCondition, pthread_mutex_t &plusMutex) {
    Sender(a);
    pthread_cond_signal(&plusCondition);
    pthread_cond_wait(&plusCondition, &plusMutex);
    Sender(b);
    pthread_cond_signal(&plusCondition);
    pthread_cond_wait(&plusCondition, &plusMutex);
    return Receiver();
}
